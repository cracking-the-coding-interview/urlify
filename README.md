1.3. URLify
======================================

### STATUS:
* master: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/urlify/badges/master/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/urlify/commits/master)
* develop: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/urlify/badges/develop/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/urlify/commits/develop)

======================================
### Table of Contents
1. [Running Tests](https://gitlab.com/cracking-the-coding-interview/urlify#running-tests)
2. [Problem Statement](https://gitlab.com/cracking-the-coding-interview/urlify#problem-statement)
3. [Questions](https://gitlab.com/cracking-the-coding-interview/urlify#questions)
4. [Solution](https://gitlab.com/cracking-the-coding-interview/urlify#solution)
5. [Implementation](https://gitlab.com/cracking-the-coding-interview/urlify#implementation)
6. [Conclusion](https://gitlab.com/cracking-the-coding-interview/urlify#implementation)
7. [Additional notes](https://gitlab.com/cracking-the-coding-interview/urlify#additional-notes)
8. [References](https://gitlab.com/cracking-the-coding-interview/urlify#references)

# Running Tests:
To run the tests, you'll need to use Cargo:
```
cargo test
```
The integration, unit, and doc tests will be run.

# Problem Statement:
Write a method to replace all spaces in a string with '%20'.
You may assume that the string has sufficient space at the end
to hold the additional characters, and that you were given the
"true" length of the string.
(Note: If implementing in Java, please use a character array
so that you can perform this operation in place.)

# Questions:
1. The note regarding implementation in Java seems to strongly
suggest that the preferred solution should perform this
operation in place.  Is this the case only for Java, or should
this be the approach irregardless of implementation language?

2. Will these strings be ASCII, or Unicode strings?  Will they
contain Unicode characters in the astral planes?

3. What character will be used to denote the padding space
at the end of these strings?  If the strings have sufficient
space at the end to hold the additional characters, will these
characters be spaces too?  Or will they be unprintable characters?  The example seems to imply that they are spaces.

4. Does the 'in place' requirement mean that no new memory
should be allocated for the string?  This seems to say that
I should pay attention to the language's pass by rules, to
ensure that the string is passed by reference or pointer.

5. JavaScript strings are immutable, so we can't work on the
input string directly.  But we could convert the input string
to an array, and pass that array in instead.  Would this be
acceptable, or should I look at other implementation
languages?  The instructions for the Java implementation
suggest that the input string can be converted into another
form without violating the 'in place' requirement.

# Solution:
The two assumptions given in the problem statement greatly
simplify the problem.  I don't need to worry about allocating
new memory, nor do I need to discover the length of the string.

We will return an empty string on empty string inputs.

The input string will be converted into an Unicode character
array before processing.

We can iterate through the array linearly.  If we see a
space character, we can shift every character after the space
to the right by two spaces.  This gives us three spots
(the original space is the first) for the three character
'%20' string to insert.  We can write this three character
string in, and continue processing.  We keep going until
we hit the end of the array.  If we've done this correctly,
we should be right at the end of the allocated space for the array, with no extra padding.

We then convert the Unicode character array into a string, and
return that string.

# Implementation:
I'm going to use Rust.  I will need a helper function to take the
input string and convert it into an Vector of chars, and add in
the required padding spaces.

I'd like to have two functions.  One for iterating through the
string, and the second to perform the shifting and inserts.

# Conclusion:
After completing implementation, I took a look at the author's
solution to this problem.  Her solution is quite a bit better
than mine.  Editing the string is done in reverse, from end
to start, thus avoiding the need to shift characters to make
space for the "%20" string.  This reverse processing shifts
characters directly into their final positions, as opposed to
an intermediate position.

I also did not take advantage of the assumptions allowed by the
problem, and created a series of helper functions to format
the input into the required format.

The v1.0.0 tag will point towards my original solution.

I'm going to modify my solution to:
1. Remove unneeded helper functions, and exploit the allowed
assumptions.
2. Redo shift_and_fill to use a reverse processing approach
similar to the author's solution.

The v2.0.0 tag will point to the completed refactored solution.

Why am I not just removing my solution, even though it's not a
good solution?
Because when I make a mistake, I accept it, report it, learn
from it, and fix them.  Nobody is perfect.  The original
solution is also being left as a reference point.

# Additional notes:

# References

