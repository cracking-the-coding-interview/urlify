/// Takes a string, and returns a urlified string.
/// This problem only requires us to replace spaces with
/// "%20" strings.
///
/// If input_string is an empty string, we return an empty string.
///
/// The examples in the book show trailing whitespace being used to
/// reserve space at the end of strings for the additional
/// characters.
///
/// # Arguments
///
/// * `input` - A String containing data to be urlified.
/// 			Assume that this string is already prepared.
///				(has additional trailing spaces)
/// * `true_length` - The true length of the string, minus
///					  trailing spaces.
///
/// # Example
///
/// ```
/// let input_string: String = String::from("Hello World  ");
/// let length: usize = 11;
/// let output: String = urlify::urlify(&input_string, &length);
/// ```
pub fn urlify(input_string: &String, true_length: &usize) -> String {
	if input_string.len() == 0 {
		return String::from("");
	}

	let mut prepared_vec: Vec<char> = input_string.chars().collect();

	let mut i: usize = true_length - 1;
	let mut j: usize = prepared_vec.len() - 1;

	while j > i {
		if prepared_vec[i] == ' ' {
			prepared_vec[j] = '0';
			prepared_vec[j-1] = '2';
			prepared_vec[j-2] = '%';
			i -= 1;
			j -= 3;
		} else {
			prepared_vec[j] = prepared_vec[i];
			prepared_vec[i] = ' ';
			i -= 1;
			j -= 1;
		}
	}

	return prepared_vec.into_iter().collect();
}

#[cfg(test)]
mod tests {
	use super::*;

	// Generators for test functions.
	fn get_input_strings() -> Vec<String> {
		let input_strings: Vec<String> = vec![
			String::from("Hello World  "), // 1 space, 2 pads.  True length: 11.
			String::from("This string has many spaces        "), // 4 spaces, 8 pads.  True length: 27.
			String::from("This string has   multiple    spaces                  "), // 9 spaces, 18 pads.  True length: 36.
			String::from("Iñt ërnâtiônàl izætiøn ☃   💩            ") // 6 spaces, 12 pads.  True length: 28.
		];

		return input_strings;
	}

	fn get_input_string_true_lengths() -> Vec<usize> {
		let input_string_true_lengths: Vec<usize> = vec![11, 27, 36, 28];

		return input_string_true_lengths;
	}

	fn get_output_strings() -> Vec<String> {
		let input_strings: Vec<String> = vec![
			String::from("Hello%20World"),
			String::from("This%20string%20has%20many%20spaces"),
			String::from("This%20string%20has%20%20%20multiple%20%20%20%20spaces"),
			String::from("Iñt%20ërnâtiônàl%20izætiøn%20☃%20%20%20💩")
		];

		return input_strings;
	}

	// Integration tests.
	mod urlify_tests {
		use super::*;

		#[test]
		fn should_return_empty_string_on_empty_string_input() {
			let input_string: String = String::from("");
			let true_length = input_string.len();
			let actual = urlify(&input_string, &true_length);

			assert_eq!(actual, String::from(""));
		}

		#[test]
		fn should_urlify_strings_correctly() {
			let input_strings: Vec<String> = get_input_strings();
			let input_string_true_lengths: Vec<usize> = get_input_string_true_lengths();
			let output_strings: Vec<String> = get_output_strings();

			let mut i: usize = 0;
			while i < input_strings.len() {
				let actual = urlify(&input_strings[i], &input_string_true_lengths[i]);
				println!("actual: {}, input_strings[{}]: {}", actual, i, output_strings[i]);
				assert_eq!(actual, output_strings[i]);
				i += 1;
			}
		}
	}
}
